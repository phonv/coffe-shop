import React from 'react';
import AppNavigation from './src/navigation/AppNavigation';
import ConfigStore from './src/redux/ConfigStore';
import {Provider} from 'react-redux';
const store = ConfigStore();
const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <AppNavigation />
    </Provider>
  );
};

export default App;
