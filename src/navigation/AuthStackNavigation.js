import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import LoginScreen from '../containers/AuthScreen/Login';
import ForgotScreen from '../containers/AuthScreen/Forgot';
const AuthStack = createStackNavigator();
const AuthStackScreen = ({navigation}) => (
  <AuthStack.Navigator
    headerMode={'none'}
    screenOptions={{
      headerTintColor: 'white',
      headerTitleAlign: 'center',
      headerStyle: {backgroundColor: '#C5B08E'},
    }}>
    <AuthStack.Screen name="Login" component={LoginScreen} />
    <AuthStack.Screen name="Forgot" component={ForgotScreen} />
  </AuthStack.Navigator>
);
export default AuthStackScreen;
