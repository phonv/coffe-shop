import 'react-native-gesture-handler';
import React, {useState, useEffect} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import HomeStack from './HomeStackNavigation';
import ProfileStack from './ProfileStackNavigation';
import AuthStack from './AuthStackNavigation';
import {useDispatch, useSelector} from 'react-redux';
import * as Action from './../redux/Actions';
import firebase from 'react-native-firebase';
import SplashScreen from 'react-native-splash-screen';
const Drawer = createDrawerNavigator();

export default function AppNavigation() {
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const userStore = useSelector(state => state.Auth.user);
  useEffect(() => {
    const a = firebase.auth().currentUser;
    const auth = firebase.auth().onUserChanged(user => {
      if (user) {
        SplashScreen.hide();
        dispatch(Action.receiveLogin(user));
      }
    });
    auth();
    return () => {
      auth();
    };
  });
  const isAuthenticated = useSelector(state => state.Auth.isAuthenticated);
  useEffect(() => {
    SplashScreen.hide();
  });
  return (
    <NavigationContainer>
      {isAuthenticated ? (
        <Drawer.Navigator
          initialRouteName="Home"
          drawerType="front"
          contentOptions={
            ({activeTintColor: '#000000'}, {activeBackgroundColor: '#000000'})
          }>
          <Drawer.Screen name="Home" component={HomeStack} headerTitle="Home" />
          <Drawer.Screen name="Profile" component={ProfileStack} />
        </Drawer.Navigator>
      ) : (
        <AuthStack />
      )}
    </NavigationContainer>
  );
}
