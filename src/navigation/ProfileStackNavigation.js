import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Button, View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import ProfileScreen from '../containers/ProfileScreen/ProfileScreen';
import ProfileChildScreen from '../containers/ProfileScreen/ProfileChildScreen';
const ProfileStack = createStackNavigator();
const ProfileStackNavigation = () => (
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={ProfileScreen} />
    <ProfileStack.Screen name="Profile Child" component={ProfileChildScreen} />
  </ProfileStack.Navigator>
);
export default ProfileStackNavigation;
