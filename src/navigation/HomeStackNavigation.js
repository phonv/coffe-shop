import * as React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {Button, View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import HomeScreen from '../containers/HomeScreen/HomeScreen';
import HomeChildScreen from '../containers/HomeScreen/HomeChildScreen';
const HomeStack = createStackNavigator();
const HomeStackScreen = ({navigation}) => (
  <HomeStack.Navigator>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{
        headerLeft: () => (
          <Entypo
            name="menu"
            size={40}
            color="#000000"
            onPress={() => navigation.openDrawer()}
          />
        ),
      }}
    />
    <HomeStack.Screen name="Home Child" component={HomeChildScreen} />
  </HomeStack.Navigator>
);
export default HomeStackScreen;
