import React from 'react';
import {View, Text} from 'react-native';
function SplashScreen(props) {
  return (
    <View style={{flex:1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Loading...</Text>
    </View>
  );
}
export default SplashScreen;
