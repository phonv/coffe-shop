import React from 'react';

const errors = new Map();
errors.set('auth/invalid-email', {EmailErr: 'Email not correct', PassErr: ''});
errors.set('auth/email-already-in-use', {
  EmailErr: 'Email is already in use, try an orther email',
  PassErr: '',
});
errors.set('auth/user-not-found', {
  EmailErr: 'Email has not been registered',
  PassErr: '',
});
errors.set('auth/user-disabled', {
  EmailErr: 'Email has been disabled, Contact an administrator to enable',
  PassErr: '',
});
errors.set('auth/weak-password', {
  EmailErr: '',
  PassErr: 'Password minimum length is 8 characters',
});
errors.set('auth/wrong-password', {
  EmailErr: '',
  PassErr: 'Password is incorrect, try again!',
});
errors.set('auth/required-password', {
  EmailErr: '',
  PassErr: 'Password can not empty!',
});
errors.set('auth/required-re_password', {
  EmailErr: '',
  ErrType: 'register',
  RePassErr: 'RePassword can not empty!',
  PassErr: '',
});
errors.set('auth/required-email', {
  EmailErr: 'Email can not empty!',
  PassErr: '',
});
errors.set('auth/wrong-re_password', {
  EmailErr: '',
  ErrType: 'register',
  RePassErr: 'RePassword is not correct, try again!',
  PassErr: '',
});
errors.set('auth/too-many-requests', {
  EmailErr: '',
  PassErr: 'Enter the password incorrectly too many times!',
});
errors.set('auth/account-exists-with-different-credential', {
  EmailErr: '',
  ErrType: 'social',
  PassErr:
    'The account is exist with different credential, try again with your email!',
});
// errors.set('',{ EmailErr: '',PassErr : 'An error occurred'});

const Validate = errCode => {
  // console.log(errCode);
  const check_exist = errors.has(errCode);
  if (check_exist == true) {
    return errors.get(errCode);
  } else {
    return {EmailErr: '', PassErr: 'An error occurred'};
  }
};

export default Validate;
