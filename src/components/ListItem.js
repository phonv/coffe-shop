import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
const Styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
  },
  img: {
    width: 64,
    height: 64,
  },
});

const ListItem = props => {
  return (
    <View style={Styles.container}>
      <Text>{props.title}</Text>
      <Image style={Styles.img} source={props.url} />
    </View>
  );
};

export default ListItem;
