import React from 'react';
import {Text, View} from 'react-native';
import {Overlay} from 'react-native-elements';
import {DotIndicator} from 'react-native-indicators';
const LoginLoading = props => {
  return (
    <Overlay
      isVisible={true}
      borderRadius={7}
      fullScreen
      overlayStyle={{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      {/*<View style={{backgroundColor: '#f6f7f9', opacity: 0.4}}>*/}
      <DotIndicator color="#C5B08E" size={14} count={5} animating />
      {/*</View>*/}
    </Overlay>
  );
};

export default LoginLoading;
