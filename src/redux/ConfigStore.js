import {applyMiddleware, compose, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import rootReducer from './reducers/index';
const middleware = [thunkMiddleware];
const configureStore = persistedState => {
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  const store = createStore(
    rootReducer,
    persistedState,
    composeEnhancers(applyMiddleware(...middleware)),
  );
  return store;
};
export default configureStore;
