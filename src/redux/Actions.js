import * as types from './Constants';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import firebase from 'react-native-firebase';

export const receiveLogin = user => {
  return {
    type: types.LOGIN_SUCCESS,
    user,
  };
};

const loginError = err_code => {
  return {
    type: types.LOGIN_FAILURE,
    err_code: err_code,
  };
};
const receiveSignUp = () => {
  return {
    type: types.SIGNUP_SUCCESS,
  };
};

const signUpError = err_code => {
  return {
    type: types.SIGNUP_FAILURE,
    err_code: err_code,
  };
};
export const receiveLogout = () => {
  return {
    type: types.LOGOUT_SUCCESS,
  };
};

const verifyRequest = () => {
  return {
    type: types.VERIFY_REQUEST,
  };
};

const verifySuccess = () => {
  return {
    type: types.VERIFY_SUCCESS,
  };
};
const sendMailSuccess = () => {
  return {
    type: types.SENDMAIL_SUCCESS,
  };
};
const sendMailErr = err_code => {
  console.log(err_code);
  return {
    type: types.SENDMAIL_FAILURE,
    err_code: err_code,
  };
};
