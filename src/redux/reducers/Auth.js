import * as type from './../Constants';
export default (
  state = {
    isVerifying: false,
    loginError: false,
    errCode: '',
    signUpError: false,
    logoutError: false,
    isSendmail: false,
    sendMailError: false,
    isAuthenticated: false,
    user: {},
  },
  action,
) => {
  switch (action.type) {
    case type.LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
        user: action.user,
        errCode: '',
      };

    case type.LOGIN_FAILURE:
      return {
        ...state,
        isAuthenticated: false,
        loginError: true,
        errCode: action.err_code,
      };
    case type.SIGNUP_SUCCESS:
      return {
        ...state,
        signUpError: false,
        errCode: '',
        SignUpSuss: true,
      };
    case type.SIGNUP_FAILURE:
      return {
        ...state,
        signUpError: true,
        errCode: action.err_code,
      };
    case type.LOGOUT_SUCCESS:
      return {
        ...state,
        isAuthenticated: false,
        user: {},
      };
    case type.LOGOUT_FAILURE:
      return {
        ...state,
        logoutError: true,
      };
    case type.VERIFY_REQUEST:
      return {
        ...state,
        isVerifying: true,
        verifyingError: false,
      };
    case type.VERIFY_SUCCESS:
      return {
        ...state,
        isVerifying: false,
      };
    case type.SENDMAIL_FAILURE:
      return {
        ...state,
        isSendmail: false,
        sendMailError: true,
        errCode: action.err_code,
      };
    case type.SENDMAIL_SUCCESS:
      return {
        ...state,
        isSendmail: false,
        sendMailError: false,
      };
    case type.SENDMAIL_REQUEST:
      return {
        ...state,
        isSendmail: true,
      };
    default:
      return state;
  }
};
