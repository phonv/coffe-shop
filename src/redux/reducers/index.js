import Auth from './Auth';
import {combineReducers} from 'redux';
const myReducer = combineReducers({
  Auth,
});
export default myReducer;
