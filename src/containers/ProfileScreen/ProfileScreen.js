import * as React from 'react';
import {Image, Text, View, StyleSheet, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
const ProfileScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button
        onPress={() =>
          navigation.push('Profile Child', {
            name: 'Profile Screen Child',
          })
        }
        title="Go Child"
      />
    </View>
  );
};
export default ProfileScreen;
