import * as React from 'react';
import {Image, Text, View, StyleSheet, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import firebase from 'react-native-firebase';
import {useDispatch, useSelector} from 'react-redux';
import * as Action from './../../redux/Actions';
const ProfileChildScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(state => state.Auth.isAuthenticated);
  const signOut = async () => {
    try {
      await firebase.auth().signOut();
      dispatch(Action.receiveLogout());
    } catch (e) {
      console.log(e);
    }
  };
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button onPress={signOut} title="Sign Out " />
    </View>
  );
};
export default ProfileChildScreen;
