import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView, StyleSheet, Image} from 'react-native';
import IconFAW from 'react-native-vector-icons/FontAwesome';
import {Button, Input} from 'react-native-elements';
import firebase from 'react-native-firebase';
import AwesomeAlert from 'react-native-awesome-alerts';
import Validate from '../../components/Validate';
import logo from '../../public/img/logoDark.png';

const Forgot = props => {
  const [emailForgot, setEmailForgot] = useState('');
  const [error, setError] = useState('');
  const [errorForm, setErrorForm] = useState({});
  const [dialog, setDialog] = useState({show: false, message: ''});
  useEffect(() => {
    if (error !== '') {
      let err = Validate(error);
      setErrorForm(err);
      setError('');
    }
  }, [error]);
  const handleSendMail = async () => {
    if (emailForgot == '') {
      setError('auth/required-email');
      return;
    }
    try {
      await firebase.auth().sendPasswordResetEmail(emailForgot);
      setDialog({
        show: true,
        message:
          'PassWord Reset Email has sent, please check your email and sign in!',
      });
    } catch (e) {
      setError(e.code);
    }
  };
  return (
    <ScrollView
      contentContainerStyle={{flex: 1, justifyContent: 'space-around'}}>
      <View
        style={{
          width: '100%',
          height: '40%',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Image style={{width: 150, height: 150}} source={logo} />
      </View>
      <View
        style={{
          width: '100%',
          height: '60%',
          alignItems: 'center',
          paddingHorizontal: 30,
        }}>
        <Text style={styles.titleScreen}>Forgot Password</Text>
        <Input
          placeholder="Your Email"
          errorMessage={errorForm.EmailErr ? errorForm.EmailErr : ''}
          leftIcon={<IconFAW name="user" size={18} color="black" />}
          value={emailForgot}
          onChangeText={email => setEmailForgot(email)}
        />
        <Button
          type="solid"
          buttonStyle={styles.buttonSend}
          title="Send Password"
          onPress={handleSendMail}
        />
      </View>
      <AwesomeAlert
        show={dialog.show ? true : false}
        showConfirmButton
        contentContainerStyle={styles.containAlert}
        showProgress={false}
        message={dialog.message}
        messageStyle={styles.messageAlert}
        confirmButtonTextStyle={styles.buttonAlertText}
        confirmText="Continue Sign In"
        onConfirmPressed={() => {
          props.navigation.goBack();
        }}
        confirmButtonStyle={styles.AccessButton}
      />
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  buttonSend: {
    marginTop: 30,
  },
  titleScreen: {
    fontSize: 24,
    marginBottom: 30,
  },
  messageAlert: {
    fontSize: 14,
    textAlign: 'center',
  },
  containAlert: {
    width: 350,
    paddingBottom: 10,
  },
  AccessButton: {
    backgroundColor: '#15C53F',
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonAlertText: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});

export default Forgot;
