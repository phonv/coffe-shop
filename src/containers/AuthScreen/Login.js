import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Image,
  Text,
  TouchableOpacity,
} from 'react-native';
import {Button, Input, Divider} from 'react-native-elements';
import * as Action from './../../redux/Actions';
import {LoginManager, AccessToken} from 'react-native-fbsdk';
import firebase from 'react-native-firebase';
import {GoogleSignin} from 'react-native-google-signin';
import {useDispatch, useSelector} from 'react-redux';
import _ from 'lodash';
import IconFAW from 'react-native-vector-icons/FontAwesome';
import Loading from './../../components/LoginLoading';
import logo from './../../public/img/logoDark.png';
import Validate from '../../components/Validate';
import AwesomeAlert from 'react-native-awesome-alerts';
import Dialog from 'react-native-dialog';
let LoginScreen;
LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const [user, setUser] = useState({});
  const [loading, setLoading] = useState(false);
  const [isSignUp, setIsSignUp] = useState(false);
  // const [accessSignUp, setAccessSignUp] = useState(false);
  const [userLogin, setUserLogin] = useState({
    email: '',
    password: '',
    rePassword: '',
  });
  const [Dialog, setDialog] = useState({
    type: '',
    message: '',
  });
  const [errorForm, setErrorForm] = useState({});
  const [errorMessage, setErrorMessage] = useState('');
  useEffect(() => {
    if (errorMessage !== '') {
      let err = Validate(errorMessage);
      if (err.ErrType && err.ErrType == 'social') {
        setDialog({type: 'error', message: err.PassErr});
        setErrorMessage('');
        return;
      }
      setErrorForm(err);
      setErrorMessage('');
    }
  }, [errorMessage]);
  const handleRegister = () => {
    if (userLogin.email == '') {
      setErrorMessage('auth/required-email');
      return;
    }
    if (userLogin.password == '') {
      setErrorMessage('auth/required-password');
      return;
    }
    if (userLogin.rePassword == '') {
      setErrorMessage('auth/required-re_password');
      return;
    }
    if (userLogin.password !== userLogin.rePassword) {
      setErrorMessage('auth/wrong-re_password');
      return;
    }
    register(userLogin.email, userLogin.password);
  };
  const handleLogin = () => {
    if (userLogin.email != '' && userLogin.password != '') {
      login(userLogin.email, userLogin.password);
    }
  };

  const register = async (email, password) => {
    try {
      const firebaseUserCredential = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);
      setIsSignUp(false);
      setErrorForm({});
      setDialog({type: 'access', message: 'Sign Up successfully!'});
      await firebase.auth().signOut();
    } catch (e) {
      setErrorMessage(e.code);
    }
  };
  const login = async (email, password) => {
    try {
      setLoading(true);
      const firebaseUserCredential = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      setUser(firebaseUserCredential.user);
    } catch (e) {
      setErrorMessage(e.code);
      setLoading(false);
    }
  };
  const googleLogin = async () => {
    try {
      await GoogleSignin.configure({
        scopes: ['https://www.googleapis.com/auth/drive.readonly'],
        webClientId:
          '811785302393-chb9h0t950bm57s5vlpaevq244n0bn5g.apps.googleusercontent.com',
        offlineAccess: true,
        hostedDomain: '',
        loginHint: '',
        forceConsentPrompt: true,
        accountName: '',
      });
      const data = await GoogleSignin.signIn();
      setLoading(true);
      const credential = firebase.auth.GoogleAuthProvider.credential(
        data.idToken,
        data.accessToken,
      );
      const firebaseUserCredential = await firebase
        .auth()
        .signInWithCredential(credential);
      if (firebaseUserCredential.user != null) {
        setUser(firebaseUserCredential.user);
      }
    } catch (e) {
      setErrorMessage(e.code);
      setLoading(false);
    }
  };
  const facebookLogin = async () => {
    try {
      const result = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);
      if (result.isCancelled) {
        throw new Error('User cancelled request');
      }
      const data = await AccessToken.getCurrentAccessToken();
      setLoading(true);
      if (!data && data != null) {
        throw new Error(
          'Something went wrong obtaining the users access token',
        );
      }
      const credential = firebase.auth.FacebookAuthProvider.credential(
        data.accessToken,
      );
      const firebaseUserCredential = await firebase
        .auth()
        .signInWithCredential(credential);

      if (firebaseUserCredential.user != null) {
        setUser(firebaseUserCredential.user);
      }
    } catch (e) {
      setLoading(false);
      setErrorMessage(e.code);
    }
  };
  const signUp = () => {
    if (!isSignUp) {
      setIsSignUp(true);
      setErrorForm({});
      return;
    }
    setIsSignUp(false);
  };
  useEffect(() => {
    if (!_.isEmpty(user)) {
      dispatch(Action.receiveLogin(user));
    }
  }, [dispatch, user]);
  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <ScrollView
          // style={{justifyContent: 'space-between'}}
          contentContainerStyle={{
            flex: 1,
            justifyContent: 'space-evenly',
          }}>
          {/*<View>*/}
          <View
            style={{
              width: '100%',
              height: '40%',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image style={{width: 150, height: 150}} source={logo} />
          </View>
          <View
            style={{
              width: '100%',
              height: '60%',
              paddingHorizontal: 20,
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                height: {isSignUp} ? '70%' : '60%',
                justifyContent: 'center',
              }}>
              <Input
                placeholder="Username"
                errorMessage={errorForm.EmailErr ? errorForm.EmailErr : ''}
                leftIcon={<IconFAW name="user" size={18} color="black" />}
                value={userLogin.email}
                onChangeText={email =>
                  setUserLogin(prevState => ({
                    ...prevState,
                    email,
                  }))
                }
              />
              <Input
                placeholder="Password"
                secureTextEntry={true}
                errorMessage={errorForm.PassErr ? errorForm.PassErr : ''}
                leftIcon={<IconFAW name="lock" size={18} color="black" />}
                value={userLogin.password}
                onChangeText={password =>
                  setUserLogin(prevState => ({
                    ...prevState,
                    password,
                  }))
                }
              />
              {isSignUp ? (
                <Input
                  placeholder="Re Password"
                  secureTextEntry={true}
                  errorMessage={errorForm.RePassErr ? errorForm.RePassErr : ''}
                  leftIcon={<IconFAW name="lock" size={18} color="black" />}
                  value={userLogin.rePassword}
                  onChangeText={rePassword =>
                    setUserLogin(prevState => ({
                      ...prevState,
                      rePassword,
                    }))
                  }
                />
              ) : (
                <></>
              )}
              <Button
                type="solid"
                title={isSignUp ? 'Register' : 'Login'}
                buttonStyle={
                  isSignUp ? styles.ButtonRegister : styles.ButtonLogin
                }
                onPress={isSignUp ? handleRegister : handleLogin}
              />
            </View>
            <View
              style={{
                height: {isSignUp} ? '30%' : '40%',
                justifyContent: 'space-around',
              }}>
              {isSignUp ? (
                <></>
              ) : (
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '60%',
                    paddingHorizontal: 10,
                  }}>
                  <View style={{flex: 1}}>
                    <Button
                      type="solid"
                      buttonStyle={styles.ButtonLoginFB}
                      icon={
                        <IconFAW
                          name="facebook-square"
                          color="#ffffff"
                          size={30}
                        />
                      }
                      onPress={facebookLogin}
                    />
                  </View>
                  <View style={{flex: 1}}>
                    <Button
                      type="solid"
                      buttonStyle={styles.ButtonLoginGG}
                      icon={
                        <IconFAW
                          name="google-plus-square"
                          color="#ffffff"
                          size={30}
                        />
                      }
                      onPress={googleLogin}
                    />
                  </View>
                </View>
              )}

              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  marginHorizontal: 10,
                  height: '40%',
                }}>
                <TouchableOpacity onPress={() => navigation.push('Forgot')}>
                  <Text style={styles.TextSignUp}>
                    <IconFAW
                      name="chevron-circle-left"
                      size={15}
                      color="#1970C5"
                      style={{paddingRight: 10}}
                    />
                    {'    '}Forgot Password
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => signUp()}>
                  <Text style={styles.TextSignUp}>
                    {isSignUp ? 'Sign In' : 'Create New Account'}
                    {'    '}
                    <IconFAW
                      name="chevron-circle-right"
                      size={15}
                      color="#1970C5"
                    />
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          {/*<View>*/}
          {/*  <Dialog.Container visible={Dialog.type != '' ? true : false}>*/}
          {/*    <Dialog.Description>{Dialog.message}</Dialog.Description>*/}
          {/*    <Dialog.Button*/}
          {/*      label="OK"*/}
          {/*      onPress={() => {*/}
          {/*        setDialog({type: '', message: ''});*/}
          {/*      }}*/}
          {/*    />*/}
          {/*  </Dialog.Container>*/}
          {/*</View>*/}
          <AwesomeAlert
            show={Dialog.type != '' ? true : false}
            showConfirmButton
            contentContainerStyle={styles.containAlert}
            showProgress={false}
            message={Dialog.message}
            messageStyle={styles.messageAlert}
            confirmButtonTextStyle={styles.buttonAlertText}
            confirmText={
              Dialog.type == 'error' ? 'OK, I know!' : 'Continute Sign In'
            }
            onConfirmPressed={() => {
              setDialog({type: '', message: ''});
            }}
            confirmButtonStyle={
              Dialog.type != '' && Dialog.type == 'error'
                ? styles.ErrorButton
                : styles.AccessButton
            }
          />
          {/*</View>*/}
        </ScrollView>
      )}
    </>
  );
};
const styles = StyleSheet.create({
  ButtonLoginFB: {
    marginRight: 20,
    backgroundColor: '#3B5998',
  },
  ButtonLoginGG: {
    backgroundColor: '#ee0b2a',
  },
  ButtonLogin: {
    marginHorizontal: 10,
    marginTop: 20,
    height: 50,
    backgroundColor: '#C5B08E',
  },
  TextSignUp: {
    fontSize: 15,
    color: '#1970C5',
  },
  ButtonRegister: {
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 10,
    height: 50,
    backgroundColor: '#15C53F',
  },
  paddingTitle: {
    paddingLeft: 10,
  },
  messageAlert: {
    fontSize: 14,
    textAlign: 'center',
  },
  containAlert: {
    width: 350,
    paddingBottom: 10,
  },
  ErrorButton: {
    backgroundColor: 'red',
    width: 100,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  AccessButton: {
    backgroundColor: '#15C53F',
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonAlertText: {
    textTransform: 'uppercase',
    fontWeight: 'bold',
  },
});
export default LoginScreen;
