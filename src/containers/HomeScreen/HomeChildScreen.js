import * as React from 'react';
import {Image, Text, View, StyleSheet, Button} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Entypo from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/FontAwesome';

const HomeChildScreen = ({navigation}) => {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Child</Text>
      <Button onPress={() => navigation.goBack()} title="Go back " />
    </View>
  );
};
export default HomeChildScreen;
