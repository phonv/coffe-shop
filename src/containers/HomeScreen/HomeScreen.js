import * as React from 'react';
import {
  Image,
  Text,
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import Entypo from 'react-native-vector-icons/Entypo';
import {Button, Input} from 'react-native-elements';
import IconFAW from 'react-native-vector-icons/FontAwesome';
import logo from './../../public/img/logoDark.png';
const HomeScreen = ({navigation}) => {
  return (
    <ScrollView style={{flex: 1}}>
      <View style={{flex: 1}}>
        <View
          style={{
            flex: 5,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity onPress={() => navigation.push('Home Child')}>
            <Image style={{width: 180, height: 180}} source={logo} />
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};
export default HomeScreen;
